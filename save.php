<?php
/**
 *
 *
 *	@module			imagegallery
 *	@author			Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner,cms-lab
 *	@copyright		2004-2019 Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner, cms-lab
 *	@version		see info.php of this module
 *	@link			https://gitlab.com/labby/imagegallery
 *	@license		GNU General Public License
 *	@platform		see info.php of this module
 *	@license_terms	please see info.php of this module 
 *
*/

require('../../config/config.php');
require(LEPTON_PATH.'/modules/admin.php');

// Tells script to update when this page was last updated
$update_when_modified = true; 

$database = LEPTON_database::getInstance();

$oRequest = LEPTON_request::getInstance();

$aLookUpFields = [
    "maxpics"   => ["type"  => "integer+",  "default" => 9],
    "thumbdir"  => ["type"  => "string",    "default" => "thumbs"],
    "thumbsize" => ["type"  => "integer+",  "default" => 150],
    "filenames" => ["type"  => "integer",  "default" => 0], // checkboxvalue!
    "subdirs"   => ["type"  => "integer",  "default" => 0], // checkboxvalue!
    "title"     => ["type"  => "integer",  "default" => 0], // checkboxvalue!
    "maxwidth"  => ["type"  => "integer",  "default" => 500],
    "showoriginal"  => ["type"  => "integer",  "default" => 0], // checkboxvalue!
    "textlink"  => ["type"  => "integer",  "default" => 0], // checkboxvalue!
    "picdir"    => ["type"  => "string",  "default" => ""],
    "bg"        => ["type"  => "string",  "default" => "FFFFFF"],
    "titletext" => ["type"  => "string",  "default" => "Gallery"], // -> Sprachdatei!
    "inline"    => ["type"  => "integer",  "default" => 0] // checkboxvalue!
];

$aValues = $oRequest->testPostValues( $aLookUpFields );

$database->build_and_execute(
    "update",
    TABLE_PREFIX."mod_imagegallery_settings",
    $aValues,
    "section_id=".$section_id
); 

// Check if there is a database error, otherwise say successful
if($database->is_error()) {
    $admin->print_error($database->get_error(), $js_back);
} else {
    $admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}

// Print admin footer
$admin->print_footer();

?>