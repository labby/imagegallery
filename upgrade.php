<?php
/**
 *
 *
 *	@module			imagegallery
 *	@author			Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner,cms-lab
 *	@copyright		2004-2019 Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner, cms-lab
 *	@version		see info.php of this module
 *	@link			https://gitlab.com/labby/imagegallery
 *	@license		GNU General Public License
 *	@platform		see info.php of this module
 *	@license_terms	please see info.php of this module 
 *
*/

if(!defined('LEPTON_PATH')) { exit("Cannot access this file directly"); }

$database->query("ALTER TABLE `".TABLE_PREFIX."mod_imagegallery_settings` ADD `titletext` TEXT NOT NULL ");
$database->query("ALTER TABLE `".TABLE_PREFIX."mod_imagegallery_settings` ADD `inline` INT NOT NULL DEFAULT '1'");

?>