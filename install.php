<?php
/**
 *
 *
 *	@module			imagegallery
 *	@author			Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner,cms-lab
 *	@copyright		2004-2019 Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner, cms-lab
 *	@version		see info.php of this module
 *	@link			https://gitlab.com/labby/imagegallery
 *	@license		GNU General Public License
 *	@platform		see info.php of this module
 *	@license_terms	please see info.php of this module 
 *
*/

// prevent this file from being accessed directly
if(!defined('LEPTON_PATH')) die(header('Location: index.php'));
	
// Create table
$database->query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_imagegallery_settings`");
$mod_imagegallery = 'CREATE TABLE `'.TABLE_PREFIX.'mod_imagegallery_settings` ('
		. ' `section_id` INT NOT NULL DEFAULT \'0\','
		. ' `page_id` INT NOT NULL DEFAULT \'0\','
		. ' `maxpics` INT NOT NULL DEFAULT \'0\','
		. ' `thumbdir` TEXT NOT NULL ,'
		. ' `thumbsize` INT NOT NULL DEFAULT \'0\','
		. ' `filenames` INT NOT NULL DEFAULT \'0\','
		. ' `subdirs` INT NOT NULL DEFAULT \'0\','
		. ' `title` INT NOT NULL DEFAULT \'0\','
		. ' `picdir` TEXT NOT NULL,'
		. ' `bg` TEXT NOT NULL ,'
		. ' `maxwidth` INT NOT NULL DEFAULT \'0\','
		. ' `showoriginal` INT NOT NULL DEFAULT \'0\','
		. ' `textlink` INT NOT NULL DEFAULT \'0\','
		. ' `titletext` TEXT NOT NULL ,'
		. ' `inline` INT NOT NULL DEFAULT \'1\','
		. 'PRIMARY KEY (`section_id`)'
		. ')';
$database->query($mod_imagegallery);

?>