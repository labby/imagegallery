<?php
/**
 *
 *
 *	@module			imagegallery
 *	@author			Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner,cms-lab
 *	@copyright		2004-2019 Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner, cms-lab
 *	@version		see info.php of this module
 *	@link			https://gitlab.com/labby/imagegallery
 *	@license		GNU General Public License
 *	@platform		see info.php of this module
 *	@license_terms	please see info.php of this module 
 *
*/

// Insert an extra row into the database
$values = [
    "maxpics"   => "9",
    "thumbdir"  => "thumbs",
    "thumbsize" => "150",
    "filenames" => "0",
    "subdirs"   => "0",
    "title"     => "0",
    "bg"        => "FFFFFF",
    "maxwidth"  => "500",
    "showoriginal" => "0",
    "textlink"  => "0",
    "titletext" => "Gallery",
    "inline"    => "1",
    "picdir"    => "/"
];

LEPTON_database::getInstance()->build_and_execute(
    "insert",
    TABLE_PREFIX."mod_imagegallery_settings",
    $values
);

?>