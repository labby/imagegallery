<?php
/**
 *
 *
 *	@module			imagegallery
 *	@author			Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner,cms-lab
 *	@copyright		2004-2019 Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner, cms-lab
 *	@version		see info.php of this module
 *	@link			https://gitlab.com/labby/imagegallery
 *	@license		GNU General Public License
 *	@platform		see info.php of this module
 *	@license_terms	please see info.php of this module 
 *
*/

/*
 * galerie.php - a simple gallery script
 * Copyright (C) 2004  Daniel Wacker <daniel.wacker@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * --
 * This script provides a simple gallery of all images that are located
 * in the script's directory and subdirectories.
 *
 * Installation
 * Simply put this script in a folder of your web server and call it in a
 * web browser. Be sure that the script has permission to read the image
 * files and to create and write into the thumbnail folder.
 *
 * Attention:
 * This script tries to generate jpeg thumbnail files in a subfolder of the
 * gallery folder(s). The filenames look like "originalfilename.thumb.jpg".
 *
*/

$module_directory	= 'imagegallery';
$module_name		= 'Another Image Gallery';
$module_function	= 'page';
$module_version 	= '3.0.0';
$module_platform	= '4.x';
$module_delete		=  true;
$module_author		= 'Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner,cms-lab';
$module_license		= 'GNU General Public License';
$module_description = 'This module allows you to create a image Gallery using galerie.php - a simple gallery script from Daniel Wacker.';
$module_guid		= 'e211f722-2c37-44da-b7b8-67fb68b0a0d5';

?>