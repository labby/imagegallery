<?php
/**
 *
 *
 *	@module			imagegallery
 *	@author			Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner,cms-lab
 *	@copyright		2004-2019 Daniel Wacker, Matthias Gallas, Rob Smith, Manfred Fuenkner, cms-lab
 *	@version		see info.php of this module
 *	@link			https://gitlab.com/labby/imagegallery
 *	@license		GNU General Public License
 *	@platform		see info.php of this module
 *	@license_terms	please see info.php of this module 
 *
*/

// prevent this file from being accessed directly
if(!defined('LEPTON_PATH')) die(header('Location: index.php'));  

// Delete page from mod_imagegallery
$database->query("DELETE FROM `".TABLE_PREFIX."mod_imagegallery_settings` WHERE `section_id` = '$section_id'");

?>